<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Задание 2</h1>
<p>Введите сюда адрес</p>
<input id="address" name="address" type="text" size="100"/>
<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.8.0/dist/css/suggestions.min.css" type="text/css"
      rel="stylesheet"/>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--[if lt IE 10]>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.8.0/dist/js/jquery.suggestions.min.js"></script>
<script type="text/javascript">

    $("#address").suggestions({
        token: "<%= request.getAttribute("token") %>",
        type: "ADDRESS",
        count: 5,
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function (suggestion) {
            if (document.body.lastElementChild.tagName == "DIV") {
                document.body.removeChild(document.body.lastElementChild);
            }
            let text = suggestion.data.fias_id;
            let divresult = document.createElement("div");
            document.body.appendChild(divresult);
            divresult.innerText = text;
        }
    });


</script>
<p>Здесь появится код ФИАС</p>
</body>
</html>