package servlets;

import javax.servlet.annotation.WebServlet;
import java.io.IOException;

@WebServlet(urlPatterns = "/home")
public class HomeServlet extends javax.servlet.http.HttpServlet {

    public static final String TOKEN = "35840dc3dde98f80cc79207694041f2163a01855";

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        request.setAttribute("token", TOKEN);

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }
}